from django.shortcuts import render,redirect
from django.contrib.auth.models import User
from django.contrib import auth

from rest_framework.views import APIView
from .serializers import userListSerializer
from rest_framework.response import Response

class accountListing(APIView):
    def get(self,request):
        user=User.objects.all()
        serializer=userListSerializer(user,many=True)
        return Response(serializer.data)

def signup(request):
    if request.method == 'POST':
        if request.POST['password1'] == request.POST['password2']:
            try:
                User.objects.get(username=request.POST['username'])
                return render(request,'accounts/signup.html',{'error':'User already taken'})
            except User.DoesNotExist:
                user=User.objects.create_user(request.POST['username'],password=request.POST['password1'])
                auth.login(request,user)
                return redirect('home')
        else:
            return render(request,'accounts/signup.html',{'error':'Password must match'})
    else:
        return render(request,'accounts/signup.html')

def login(request):
    if request.method == 'POST':
        user = auth.authenticate(username=request.POST['username'],password=request.POST['password'])
        if user is not None:
            auth.login(request,user)
            return redirect('home')
        else:
            return render(request,'accounts/login.html',{'error':'username or password is incorrect'})
    else:
        return render(request,'accounts/login.html')

def logout(request):
    auth.logout(request)
    return redirect('home')
