from django.urls import path
from . import views
urlpatterns = [
    path('create', views.create,name='create'),
    path('<int:product_id>', views.detail,name='detail'),
    path('upVote/<int:product_id>', views.upvote,name='upVote'),
    path('api', views.productList.as_view()),

]
