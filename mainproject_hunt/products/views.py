from django.shortcuts import render,redirect,get_object_or_404
from django.contrib.auth.decorators import login_required
from .models import Product

from rest_framework.views import APIView
from .serializers import productSerializer
from rest_framework.response import Response
from rest_framework import status
class productList(APIView):
    serializer_class = productSerializer
    def get(self,request):
        allproducts=Product.objects.all()
        serializer=productSerializer(allproducts,many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = productSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
def home(request):
    products=Product.objects
    return render(request,'products/home.html',{'products':products})

@login_required(login_url="/accounts/signup")
def create(request):
    if request.method == 'POST':
        if request.POST['title'] and request.POST['body']:
            product=Product()
            product.title=request.POST['title']
            product.body=request.POST['body']
            product.hunter=request.user
            if request.POST['image']:
                product.image=request.FILES['image']
            if request.POST['icon']:
                product.icon=request.FILES['icon']
            product.save()
            return redirect('home')
        else:
            return render(request,'products/create.html',{'error':'please enter required fields'})
    else:
        return render(request,'products/create.html')

@login_required(login_url="/accounts/signup")
def detail(request,product_id):
    product=get_object_or_404(Product,pk=product_id)
    return render(request,'products/detail.html',{'product':product})

@login_required(login_url="/accounts/signup")
def upvote(request,product_id):
    product=get_object_or_404(Product,pk=product_id)
    product.votes_total+=1
    product.save()
    return redirect('home')
